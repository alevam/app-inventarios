import Vue from 'vue'
import VueRouter from 'vue-router'
import Inventarios from '../views/Inventarios.vue'
import AddInventario from '../views/addInventario.vue'
import ShowInventario from '../views/showInventario.vue'
import auth from '../auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/inventarios',
    name: 'Inventarios',
    component: Inventarios,
    meta: { requiresAuth: true }
  },
  {
    path: '/inventarios/show/:id',
    name: 'showInventario',
    component: ShowInventario,
    meta: { requiresAuth: true }
  },
  {
    path: '/inventarios/add',
    name: 'addInventario',
    component: AddInventario,
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  { path: '/', redirect: '/inventarios' }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!auth.loggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
