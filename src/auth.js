export default {
  setAuth(auth){
      localStorage.token = auth.token;
      localStorage.user = auth.user;
      this.onChange(true);
  },

  getUser () {
    return localStorage.user
  },

  getToken () {
    return 'Bearer ' + localStorage.token
  },

  logout (cb) {
    delete localStorage.token;
    delete localStorage.user;
    this.onChange(false);
  },

  loggedIn () {
    return !!localStorage.token
  },
  onChange () {}
}