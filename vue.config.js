module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: 'http://dummy-host2.example.com/', 
        pathRewrite: { "^/api/": "/api/" },
        changeOrigin: true,
        logLevel: "debug"
      }
    },
  },
}